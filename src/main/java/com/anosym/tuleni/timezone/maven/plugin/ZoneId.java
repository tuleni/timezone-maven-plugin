package com.anosym.tuleni.timezone.maven.plugin;

import javax.annotation.Nonnull;

import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Feb 11, 2017, 7:54:20 PM
 */
@Getter
@Builder
@ToString
@EqualsAndHashCode
public class ZoneId {

    /**
     * The zone id
     */
    @Nonnull
    private final String id;

    /**
     * The zone country.
     */
    @Nonnull
    private final String countryCode;

    /**
     * The zone name e.g. Africa/Nairobi
     */
    @Nonnull
    private final String name;

}
