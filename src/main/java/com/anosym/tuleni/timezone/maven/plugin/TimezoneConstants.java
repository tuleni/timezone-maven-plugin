package com.anosym.tuleni.timezone.maven.plugin;

import javax.annotation.Nonnull;

import com.google.common.base.CharMatcher;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Feb 12, 2017, 9:15:39 AM
 */
public final class TimezoneConstants {

    private static final CharMatcher QUOTE = CharMatcher.is('"').precomputed();

    public static final String TMP_WORKING_DIR = "/tmp/timezone";

    public static final String TIMEZONE_FILE = "timezone.csv";

    public static final String ZONE_FILE = "zone.csv";

    @Nonnull
    public static String unquote(@Nonnull final String str) {
        final String trimmedStr = str.trim();
        return QUOTE.trimFrom(trimmedStr);
    }

}
