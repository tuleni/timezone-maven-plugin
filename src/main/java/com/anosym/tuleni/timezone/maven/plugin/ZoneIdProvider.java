package com.anosym.tuleni.timezone.maven.plugin;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Nonnull;

import static java.util.stream.Collectors.toList;

import static com.anosym.tuleni.timezone.maven.plugin.TimezoneConstants.TMP_WORKING_DIR;
import static com.anosym.tuleni.timezone.maven.plugin.TimezoneConstants.ZONE_FILE;
import static com.anosym.tuleni.timezone.maven.plugin.TimezoneConstants.unquote;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Feb 12, 2017, 9:11:50 AM
 */
public class ZoneIdProvider {

    @Nonnull
    public List<ZoneId> getZoneIds() {
        final File file = new File(TMP_WORKING_DIR, ZONE_FILE);
        final List<ZoneId> zoneIds = new ArrayList<>();
        try (final BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(file)))) {
            String line;
            while ((line = reader.readLine()) != null) {
                zoneIds.add(buildZoneId(line));
            }
        } catch (final IOException ex) {
            throw new RuntimeException(ex);
        }

        return zoneIds.stream().distinct().collect(toList());
    }

    @Nonnull
    private ZoneId buildZoneId(@Nonnull final String line) {
        final String[] parts = line.split(",");
        return ZoneId
                .builder()
                .id(unquote(parts[0]))
                .countryCode(unquote(parts[1]))
                .name(unquote(parts[2]))
                .build();
    }

}
