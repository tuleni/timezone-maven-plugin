package com.anosym.tuleni.timezone.maven.plugin;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Nonnull;

import static java.util.stream.Collectors.toList;

import static com.anosym.tuleni.timezone.maven.plugin.TimezoneConstants.TIMEZONE_FILE;
import static com.anosym.tuleni.timezone.maven.plugin.TimezoneConstants.TMP_WORKING_DIR;
import static com.anosym.tuleni.timezone.maven.plugin.TimezoneConstants.unquote;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Feb 12, 2017, 9:11:50 AM
 */
public class TimezoneProvider {

    @Nonnull
    public List<Timezone> getTimezones() {
        final File file = new File(TMP_WORKING_DIR, TIMEZONE_FILE);
        final List<Timezone> timezones = new ArrayList<>();
        try (final BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(file)))) {
            String line;
            while ((line = reader.readLine()) != null) {
                timezones.add(buildTimezone(line));
            }
        } catch (final IOException ex) {
            throw new RuntimeException(ex);
        }

        return timezones.stream().distinct().collect(toList());
    }

    @Nonnull
    private Timezone buildTimezone(@Nonnull final String line) {
        final String[] parts = line.split(",");
        return Timezone
                .builder()
                .id(unquote(parts[0]))
                .abbreviation(unquote(parts[1]))
                .gmtOffset(Integer.parseInt(unquote(parts[3])))
                .dst(Integer.parseInt(unquote(parts[4])) == 1)
                .build();
    }

}
