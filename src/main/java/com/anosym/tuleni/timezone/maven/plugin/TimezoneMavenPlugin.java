package com.anosym.tuleni.timezone.maven.plugin;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.util.AbstractMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.function.Function;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.IntStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import javax.annotation.Nonnull;

import com.google.common.collect.ImmutableMap;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.codehaus.plexus.util.IOUtil;

import static java.lang.String.format;
import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toMap;

import static com.anosym.tuleni.timezone.maven.plugin.TimezoneConstants.TMP_WORKING_DIR;

/**
 *
 * @author marembo
 */
@Mojo(name = "timezone-js", defaultPhase = LifecyclePhase.COMPILE)
public class TimezoneMavenPlugin extends AbstractMojo {

    private static final Logger LOG = Logger.getLogger(TimezoneMavenPlugin.class.getName());

    @SuppressWarnings("FieldMayBeFinal")
    @Parameter(readonly = true, defaultValue = "${basedir}/src/main/resources")
    private String outputDirectory = "src/main/resources";

    @SuppressWarnings("FieldMayBeFinal")
    @Parameter(readonly = true, defaultValue = "timezone.js")
    private String outputFile = "timezone.js";

    @SuppressWarnings("FieldMayBeFinal")
    @Parameter(readonly = true, defaultValue = "https://timezonedb.com/files/timezonedb.csv.zip")
    private String timezoneDbUrl = "https://timezonedb.com/files/timezonedb.csv.zip";

    private final TimezoneProvider timezoneProvider = new TimezoneProvider();

    private final ZoneIdProvider zoneIdProvider = new ZoneIdProvider();

    @Override
    public void execute() throws MojoExecutionException, MojoFailureException {
        try {
            unzipTimezoneDb();

            final Map<String, ZoneId> zoneIds = zoneIdProvider
                    .getZoneIds()
                    .stream()
                    .collect(toMap(ZoneId::getId, Function.identity()));
            final Map<Integer, List<Timezone>> timezones = timezoneProvider
                    .getTimezones()
                    .stream()
                    .collect(groupingBy(Timezone::getGmtOffset));
            final ImmutableMap.Builder<Integer, Map<String, List<String>>> offsetZoneNamesAndCountryIsoCodeBuilder
                    = ImmutableMap.builder();
            timezones.forEach((offset, timezoneList) -> {
                final List<Entry<String, String>> entries = mapTimezoneToCountryAndZoneName(timezoneList, zoneIds);
                final Map<String, List<String>> offsetCountryZoneName
                        = entries
                        .stream()
                        .distinct()
                        .collect(groupingBy(Entry::getKey))
                        .entrySet()
                        .stream()
                        .collect(toMap(Entry::getKey,
                                       (entry) -> entry.getValue().stream().map(e -> e.getValue()).collect(toList())));
                offsetZoneNamesAndCountryIsoCodeBuilder.put(offset, offsetCountryZoneName);
            });

            final Map<Integer, Map<String, List<String>>> offsetZoneNamesAndCountryIsoCode
                    = offsetZoneNamesAndCountryIsoCodeBuilder.build();
            final Map<Integer, String> offsetJs = offsetZoneNamesAndCountryIsoCode
                    .entrySet()
                    .stream()
                    .collect(toMap(Entry::getKey, (entry) -> {
                               final Map<String, String> zoneNames = entry
                                       .getValue()
                                       .entrySet()
                                       .stream()
                                       .collect(toMap(Entry::getKey,
                                                      (zoneEntries) -> {
                                                          return zoneEntries
                                                          .getValue()
                                                          .stream()
                                                          .map((val) -> format("'%s'", val))
                                                          .collect(joining(",", "[", "]"));
                                                      }));
                               return toJsMapArray(zoneNames, false, 8);
                           }));
            final String jsMappedArrays = toJsMapArray(offsetJs, false, 4);

            final File outputDir = new File(outputDirectory);
            if (!outputDir.exists()) {
                outputDir.mkdirs();
            }

            try (final FileOutputStream outputStream = new FileOutputStream(new File(outputDir, outputFile))) {
                final String jsTemplate = IOUtil.toString(getClass().getResourceAsStream("/timezone.js.template"));
                final String jsValue = jsTemplate.replace("#{json}", jsMappedArrays);
                outputStream.write(jsValue.getBytes());
            }
        } catch (Exception e) {
            throw new MojoExecutionException(getClass().getName(), e);
        }
    }

    @Nonnull
    private String toJsMapArray(@Nonnull final Map<?, ?> countryZoneName,
                                final boolean quoteValue,
                                final int padding) {
        final String paddingStr = IntStream
                .range(0, padding)
                .mapToObj((i) -> " ")
                .collect(joining(""));
        return countryZoneName
                .entrySet()
                .stream()
                .map((entry) -> {
                    final Object value = entry.getValue();
                    final Object key = entry.getKey();
                    if (quoteValue) {
                        return format("\n%s'%s': '%s'", paddingStr, key, value);
                    }

                    return format("\n%s'%s': %s", paddingStr, key, value);
                })
                .collect(joining(",", "{", "}"));
    }

    @Nonnull
    private List<Entry<String, String>> mapTimezoneToCountryAndZoneName(@Nonnull final List<Timezone> timezones,
                                                                        @Nonnull final Map<String, ZoneId> zoneIds) {
        return timezones
                .stream()
                .map((timezone) -> {
                    final ZoneId zoneId = zoneIds.get(timezone.getId());
                    if (zoneId == null) {
                        return null;
                    }

                    return new AbstractMap.SimpleEntry<>(zoneId.getCountryCode(), zoneId.getName());
                })
                .filter(Objects::nonNull)
                .collect(toList());
    }

    private void unzipTimezoneDb() {
        try (final ZipInputStream zipInputStream = new ZipInputStream(new URL(timezoneDbUrl).openStream())) {
            final File workingDir = new File(TMP_WORKING_DIR);

            if (!workingDir.exists()) {
                workingDir.mkdirs();
            }

            LOG.log(Level.INFO, "Downloading timezone db to: {0}", workingDir.getAbsolutePath());

            final byte[] buffer = new byte[1024];

            ZipEntry zipEntry;
            while ((zipEntry = zipInputStream.getNextEntry()) != null) {
                final File file = new File(workingDir, zipEntry.getName());
                try (FileOutputStream outputStream = new FileOutputStream(file)) {
                    int len;
                    while ((len = zipInputStream.read(buffer)) > 0) {
                        outputStream.write(buffer, 0, len);
                    }
                }

            }
        } catch (final IOException ex) {
            throw new RuntimeException(ex);
        }
    }

}
