package com.anosym.tuleni.timezone.maven.plugin;

import javax.annotation.Nonnull;

import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Feb 12, 2017, 9:12:25 AM
 */
@Getter
@Builder
@EqualsAndHashCode
public class Timezone {

    @Nonnull
    private String id;

    @Nonnull
    private String abbreviation;

    @Nonnull
    private int gmtOffset;

    @Nonnull
    private boolean dst;

}
